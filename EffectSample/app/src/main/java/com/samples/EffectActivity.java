/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

package com.samples;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;

import com.inartar.effect.Constraints;
import com.inartar.effect.YNBeautyEffect;
import com.inartar.effect.YNCuteEffect;
import com.inartar.effect.YNDeformationEffect;
import com.inartar.effect.YNEffect;
import com.inartar.effect.YNEffectEngine;
import com.inartar.effect.YNLandscapeEffect;
import com.inartar.effect.YNLongLegEffect;
import com.inartar.effect.YNMakeupEffect;
import com.inartar.effect.YNSharpenEffect;
import com.inartar.effect.YNSlenderEffect;
import com.xiusdk.ynfacetrack.YNFace;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiu on 2017/7/18.
 */

public class EffectActivity extends CameraActivity {

    private final String TAG = "EffectActivity";
    private YNEffectEngine mEngine = null;
    private EffectAdapter.EffectItem mEffectItem = null;
    private WeakReference<YNEffect> mSelectedEffect = null;
    private WeakReference<YNSlenderEffect> mSlenderEffect = null;
    private boolean mLandscape = false;
    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLandscape = getIntent().getBooleanExtra("landscape", false);
        RecyclerView listView = (RecyclerView) findViewById(R.id.list);
        final EffectAdapter adapter = new EffectAdapter(this,  getIntent().getStringExtra("effectPath"));
        listView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        listView.setLayoutManager(linearLayoutManager);
        adapter.setOnItemClickListener(new EffectAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                if( mEngine == null)
                    return;
                adapter.setSelectIndex(position);
                adapter.notifyDataSetChanged();
                mEffectItem = adapter.itemFromIndex(position);
                didSelectModelItem(mEffectItem);
            }
        });

        SeekBar seekbar = (SeekBar) findViewById(R.id.intensitySlider);
        seekbar.setOnSeekBarChangeListener(mSeekBarChange);

        SeekBar slenderSeekbar = (SeekBar) findViewById(R.id.slenderSlider);
        slenderSeekbar.setOnSeekBarChangeListener(mSlenderSeekBarChange);
    }

    @Override
    public void onSurfaceCreated() {
        if( mEngine != null)
        {
            mEngine.Release();
            mEngine = null;
        }
        mEngine = new YNEffectEngine();
        didSelectModelItem(mEffectItem);
    }

    @Override
    public void onSurfaceChanged() {

    }

    @Override
    public void onDestorySurface() {
        if( mEngine != null)
        {
            mEngine.Release();
            mEngine = null;
        }
    }

    @Override
    public int onPreviewFrame(byte[] data, int width, int height, int orientation, boolean frontFacing, YNFace[] faces) {

        int result = mEngine.processI420BiPlanar(data, width, width, height, orientation, frontFacing ? Constraints.YN_FLIP_HORIZONTAL : 0, faces);

        return result;
    }

    private void didSelectModelItem(EffectAdapter.EffectItem item)
    {
        if(item != null && item.getEffects() != null) {
            mSelectedEffect = null;
            mSlenderEffect = null;
            final List<YNEffect> effects = new ArrayList<YNEffect>();
            for(String effectName : item.getEffects())
            {
                YNEffect effect = null;
                if( effectName.equalsIgnoreCase("cute"))
                {
                    effect = new YNCuteEffect();
                }
                else if( effectName.equalsIgnoreCase("deformation"))
                {
                    effect = new YNDeformationEffect();
                }
                else if( effectName.equalsIgnoreCase("beauty"))
                {
                    effect = new YNBeautyEffect();
                    if( !mLandscape )
                        mSelectedEffect = new WeakReference<YNEffect>(effect);
                    //effect = e;
                }
                else if( effectName.equalsIgnoreCase("landscape"))
                {
                    effect = new YNLandscapeEffect();
                    if( mLandscape )
                        mSelectedEffect = new WeakReference<YNEffect>(effect);
                }
                else if(effectName.equalsIgnoreCase("slender"))
                {
                    YNSlenderEffect e = new YNSlenderEffect();
                    if( !mLandscape )
                        mSlenderEffect = new WeakReference<YNSlenderEffect>(e);

                    effect = e;
                }
                else if(effectName.equalsIgnoreCase("sharpen"))
                {
                    effect = new YNSharpenEffect();
                }
                else if(effectName.equalsIgnoreCase("longleg"))
                {
                    effect = new YNLongLegEffect();
                }
                else if(effectName.equalsIgnoreCase( "makeup"))
                {
                    effect = new YNMakeupEffect();
                }
                if( effect != null) {
                    effect.setConfigPath(item.Path + "/" + effectName + ".json");
                    effects.add(effect);
                }
            }
            runOnDraw(new Runnable() {
                @Override
                public void run() {
                    mEngine.addEffects(effects, true);
                }
            });


            SeekBar seekbar = (SeekBar) findViewById(R.id.intensitySlider);
            if( mSelectedEffect == null || mSelectedEffect.get() == null)
            {
                seekbar.setVisibility(View.GONE);
            }
            else
            {
                seekbar.setProgress(mSelectedEffect.get().getIntensity());
                seekbar.setVisibility(View.VISIBLE);
            }


            SeekBar slenderSeekbar = (SeekBar) findViewById(R.id.slenderSlider);
            if( mSlenderEffect == null || mSlenderEffect.get() == null)
            {
                slenderSeekbar.setVisibility(View.GONE);
            }
            else
            {
                slenderSeekbar.setProgress(mSlenderEffect.get().getIntensity());
                slenderSeekbar.setVisibility(View.VISIBLE);
            }

        }else {
            Log.w(TAG, "the selected model is null!!!!");
        }
    }

    private SeekBar.OnSeekBarChangeListener mSeekBarChange = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if(mSelectedEffect != null && mSelectedEffect.get() != null)
            {
                mSelectedEffect.get().setIntensity(progress);
            }
        }
     };

    private SeekBar.OnSeekBarChangeListener mSlenderSeekBarChange = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if(mSlenderEffect != null && mSlenderEffect.get() != null)
            {
                mSlenderEffect.get().setIntensity(progress);
                mSlenderEffect.get().setEyeBigIntensity(progress);
            }
        }
    };
}
