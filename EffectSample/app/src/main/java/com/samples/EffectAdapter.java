/*----------------------------------------------------------------------------------------------
 *
 * This file is XIU's property. It contains XIU's trade secret, proprietary and
 * confidential information.
 *
 * The information and code contained in this file is only for authorized XIU employees
 * to design, create, modify, or review.
 *
 * DO NOT DISTRIBUTE, DO NOT DUPLICATE OR TRANSMIT IN ANY FORM WITHOUT PROPER AUTHORIZATION.
 *
 * If you are not an intended recipient of this file, you must not copy, distribute, modify,
 * or take any action in reliance on it.
 *
 * If you have received this file in error, please immediately notify XIU and
 * permanently delete the original and any copy of any file and any printout thereof.
 * (c) www.xiusdk.cn
 *---------------------------------------------------------------------------------------------*/

package com.samples;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonSyntaxException;
import com.utils.GsonUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class EffectAdapter extends RecyclerView.Adapter<EffectAdapter.ModelViewHolder> {

	public class EffectItem {
		public String Path;
		private String tips;
		private List<String> effects = new ArrayList<String>();
		public String getTips() {
			return this.tips;
		}

		public void setTips(String tips) {
			this.tips = tips;
		}

		public List<String> getEffects() {
			return this.effects;
		}

		public void setEffects(List<String> effects) {
			this.effects = effects;
		}
	}

	private class Item
	{
		public String DisplayName;
		public String name;
		public String Path;
	}

	private LayoutInflater mInflater;
	Context mContext;
	private int selectIndex;
	List<Item> entryList = new ArrayList<Item>();

	public EffectAdapter(Context context, String effectPath) {
		mContext = context;
		mInflater = LayoutInflater.from(context);
		try {
			AssetManager am = context.getAssets();
			if(effectPath == null || effectPath.isEmpty())
			{
				effectPath = "effects";
			}

			String[] assets = am.list(effectPath);
			for( int i = 0; i < assets.length; i++)
			{
				String path = effectPath + "/" + assets[i];
				Item item = new Item();
				item.name = assets[i];
				item.DisplayName = assets[i];
				item.Path = path;
				if( assets[i].equalsIgnoreCase("原始") ) {
					entryList.add(0, item);
				} else {
					entryList.add(item);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ModelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View convertView = mInflater.inflate(R.layout.horizontallistview_item, null);
		ModelViewHolder vh = new ModelViewHolder(convertView);
		vh.im = (ImageView) convertView.findViewById(R.id.iv_pic);
		vh.im.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		vh.title = (TextView) convertView.findViewById(R.id.tv_name);
		return vh;
	}

	@Override
	public void onBindViewHolder(ModelViewHolder holder, final int position) {

		Item res = entryList.get(position);
		View convertView = holder.itemView;
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listener.onItemClick(v, position);
			}
		});
		if (position == selectIndex) {
			convertView.setSelected(true);
		} else {
			convertView.setSelected(false);
		}
		try {
			InputStream stream = mContext.getAssets().open(res.Path + "/thumb.png");
			Bitmap bitmap = BitmapFactory.decodeStream(stream);
			holder.im.setImageBitmap(bitmap);
		} catch (IOException e) {
			e.printStackTrace();
		}
		holder.title.setText(res.name);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return entryList.size();
	}

	public void setSelectIndex(int index) {
		selectIndex = index;
	}

	public EffectItem itemFromIndex(int index) {
		if (index < entryList.size()) {
			AssetManager am = mContext.getAssets();
			Item res = entryList.get(index);
			String cfgPath = res.Path + "/params.json";
			try {
				InputStream is = am.open(cfgPath);
				EffectItem effectItem = GsonUtil.parseJsonWithGson(is, EffectItem.class);
				effectItem.Path = res.Path;
				is.close();
				return effectItem;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JsonSyntaxException e) {
				e.printStackTrace();
			}
		}

		return null;

	}

	public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
		listener = onItemClickListener;
	}

	public interface OnItemClickListener {
		void onItemClick(View view, int position);
	}

	OnItemClickListener listener;

	public class ModelViewHolder extends RecyclerView.ViewHolder {
		private TextView title;
		private ImageView im;

		public ModelViewHolder(View itemView) {
			super(itemView);
		}
	}
}
