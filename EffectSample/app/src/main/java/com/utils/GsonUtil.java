package com.utils;

import com.google.gson.Gson;

import org.apache.http.util.EncodingUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by xiu on 2017/7/18.
 */

public class GsonUtil {
    // 将Json数据解析成相应的映射对象
    public static <T> T parseJsonWithGson(String jsonData, Class<T> type) {
        Gson gson = new Gson();
        T result = gson.fromJson(jsonData, type);
        return result;
    }

    public static <T> T parseJsonWithGson(InputStream stream, Class<T> type) throws IOException {

        byte []buffer = new byte[stream.available()];
        stream.read(buffer);
        String json = EncodingUtils.getString(buffer, "utf-8");
        stream.close();
        return parseJsonWithGson(json, type);
    }
}
