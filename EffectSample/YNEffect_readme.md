============ XIUSDK 功能说明 ============

============ 注意事项 ============

1. 修改 build.gradle
classpath 'com.android.tools.build:gradle:3.2.0' 中的3.2.0为你本地的版本

2. 修改 gradle-wrapper.properties
distributionUrl=https\://services.gradle.org/distributions/gradle-4.10.1-all.zip  中的4.10.1为你本地的版本


============ XIUSDK 文件说明 ============
1. assets目录下的 effects和landscapes文件夹包含了配置文件，主要实现美颜，大眼，瘦脸，萌颜等功能
beauty.json landscape.json sharpen.json slender.json cute.json为子配置文件, 多个子配置文件组合可以实现叠加功能，见params.json

2. libs文件夹: YNEffect.aar、ynfacetrack.aar包文件

3. 第三方文件: libs/jna-min.jar 以及库文件 src/main/jniLibs/arm64-v8a/libjnidispatch.so、src/main/jniLibs/armeabi-v7a/libjnidispatch.so


============ EffectEngine 参数说明 ============
1. 定义参数列表，然后把定义的功能模块儿添加到列表里
final List<YNEffect> effects = new ArrayList<YNEffect>();

2. 美颜功能（磨皮，美白）
美颜类YNBeautyEffect，类实例化
YNBeautyEffect effect = new YNBeautyEffect();
effect.setConfigPath(item.Path + "/" +  "beauty.json");
effects.add(effect);
参数见beauty.json
{
    "intensity":45,       // 磨皮强度，值[0, 100]
    "mode":"default",     // 默认，不要改动; "mode1": fast 模式
    "whitenIntensity":35  // 美白强度，值[0, 100]
}


3. 清晰度功能
YNSharpenEffect，类实例化
YNSharpenEffect* effect = new YNSharpenEffect();
effect.setConfigPath(item.Path + "/" +  "sharpen.json");
effects.add(effect);
参数见sharpen.json
{
    "intensity":45  // 强度，值[0, 100]
}


4. 滤镜功能
滤镜类YNLandscapeEffect，类实例化
YNLandscapeEffect* effect = YNLandscapeEffect();
effect.setConfigPath(item.Path + "/" +  "landscapes.json");
effects.add(effect);
参数见landscape.json
{
    "intensity":60, // 滤镜强度，值[0, 100]
    "mode":"LUT3D", // 默认，不要改动
    "samplers":[
                "meterial/mint.png"  // 滤镜表路径，大小512x512，png格式，可以自定义
                ]
}


5. 瘦脸大眼功能
YNSlenderEffect，类实例化
YNSlenderEffect* effect = new YNSlenderEffect();
effect.setConfigPath(item.Path + "/" +  "slender.json");
参数见slender.json
{
    "intensity":60,       // 瘦脸强度，值[0, 100]
    "eyeBigIntensity":60  // 大眼强度，值[0, 100]
}

6. 长腿功能
YNLongLegEffect，类实例化
YNLongLegEffect* effect = new YNLongLegEffect();
effect.setConfigPath(item.Path + "/" +  "longleg.json");
参数见longleg.json
{
    "intensity":60,       // 强度，值[0, 100]
}

7. 萌颜功能
YNCuteEffect，类实例化
YNCuteEffect* effect = new YNCuteEffect();
effect.setConfigPath(item.Path + "/" +  "cute.json");
参数见cute.json


============ EffectEngine 调用说明 ============
private YNEffectEngine mEngine = null;
    
// 引擎初始化
if( mEngine != null) {
    mEngine.Release();
    mEngine = null;
}
mEngine = new YNEffectEngine();

// 参数设置
mEngine.addEffects(effects, true);


// 引擎调用
int result = mEngine.processI420BiPlanar(data, width, width, height, orientation, frontFacing ? Constraints.YN_FLIP_HORIZONTAL : 0, faces);



============ facesdk 调用说明 ============
private YNFaceTrack mTracker = null;

// 引擎初始化
mTracker = new YNFaceTrack(activity, 0);

// 引擎调用
final YNFace[] faces = mTracker != null ? mTracker.track(bytes, 0, size.width, size.height, size.width, 3) : null;


// 引擎销毁
if( mTracker != null ) {
    mTracker.destory();
    mTracker = null;
}


============ www.xiusdk.com ============
